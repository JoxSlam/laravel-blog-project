<?php

namespace App\Exports;

use App\Models\Post;
use Illuminate\Contracts\Queue\ShouldQueue;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class PostsExport implements FromQuery, WithHeadings, WithMapping, ShouldQueue
{
    use Exportable;
    /**
     * @return \Illuminate\Support\Collection
     */
    public function query()
    {
        return Post::with(['category', 'user']);
    }

    public function map($post): array
    {
        return [
            $post->id,
            $post->title,
            $post->content,
            $post->category->name,
            $post->user->name,
            $post->created_at,
            $post->updated_at
        ];
    }


    public function headings(): array
    {
        return [
            'ID',
            'Title',
            'Content in ' . app()->getLocale(),
            'Category Name',
            'User Name',
            'Created At',
            'Updated At'
        ];
    }
}
