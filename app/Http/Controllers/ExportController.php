<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Exports\UsersExport;
use App\Exports\PostsExport;
use App\Exports\CategoriesExport;
use Maatwebsite\Excel\Facades\Excel;

class ExportController extends Controller
{
    public function exportUsers()
    {
        (new UsersExport)->store('users-data-' . date('Y-m-d', time()) . '.csv')->chain([
            // new NotifyUserOfCompletedExport($user),
        ]);

        return response()->json([
            'status' => 200,
            'message' => 'Users data is being exported',
        ], 200);
    }

    public function exportPosts()
    {
        (new PostsExport)->store('posts-data-' . date('Y-m-d', time()) . '.csv')->chain([
            // new NotifyUserOfCompletedExport($user),
        ]);

        return response()->json([
            'status' => 200,
            'message' => 'Posts data is being exported',
        ], 200);
    }

    public function exportCategories()
    {
        (new CategoriesExport)->store('categories-data-' . date('Y-m-d', time()) . '.csv')->chain([
            // new NotifyUserOfCompletedExport($user),
        ]);

        return response()->json([
            'status' => 200,
            'message' => 'Categories data is being exported',
        ], 200);
    }
}
