<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\QueryBuilder;
use Illuminate\Support\Facades\DB;
use Spatie\Activitylog\Models\Activity;

class ActivityController extends Controller
{
    public function search(Request $request)
    {
        try {
            $activities = Activity::query();
            $activities = $activities->where(function (Builder $builder) use ($request) {
                if($request->has('log_name')) {
                    $builder->where('log_name', $request->log_name);
                }
                if($request->has('subject_id')) {
                    $builder->where('subject_id', $request->subject_id);
                }
                if($request->has('causer_id')) {
                    $builder->where('causer_id', $request->causer_id);
                }
                if($request->has('subject_type')) {
                    $builder->where('subject_type', $request->subject_type);
                }
                if($request->has('causer_type')) {
                    $builder->where('causer_type', $request->causer_type);
                }
                if($request->has('event')) {
                    $builder->where('event', 'like', '%'.$request->event.'%');
                }
            });
            return response()->json([
                'status' => 200,
                'message' => 'Activities retrieved successfully',
                'activities' => $activities->get()
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'status' => 500,
                'message' => 'Error in retrieving activities',
                'errors' => $e->getMessage(),
            ], 500);
        }
    }
}
