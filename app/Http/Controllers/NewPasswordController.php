<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\Auth\ForgotPasswordRequest;
use App\Http\Requests\Auth\ResetPasswordRequest;
use App\Models\User;
use Illuminate\Support\Facades\Password;

class NewPasswordController extends Controller
{

    /**
     * Forgot password
     *
     * @param \App\Http\Requests\Auth\ForgotPasswordRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function forgotPassword(ForgotPasswordRequest $request)
    {
        try {
            $request->validated();
            $user = User::where('email', $request->email)->first();
            if ($user) {
                $status = Password::sendResetLink(
                    $request->only('email')
                );
                // return $status === Password::RESET_LINK_SENT
                //     ? response()->json([
                //         'status' => 200,
                //         'message' => 'Password reset link sent to your email'
                //     ], 200)
                //     : response()->json([
                //         'status' => 500,
                //         'message' => 'Error in sending password reset link'
                //     ], 500);
                if ($status === Password::RESET_LINK_SENT) {
                    activity($user->name)
                        ->causedBy($user)
                        ->performedOn($user)
                        ->event('forgot_password')
                        ->log(':causer.name sent a password reset link to :subject.email');
                    return response()->json([
                        'status' => 200,
                        'message' => 'Password reset link sent to your email'
                    ], 200);
                } else {
                    return response()->json([
                        'status' => 500,
                        'message' => 'Error in sending password reset link'
                    ], 500);
                }
            } else {
                return response()->json([
                    'status' => 404,
                    'message' => 'User not found'
                ]);
            }
        } catch (\Exception $e) {
            return response()->json([
                'status' => 500,
                'message' => 'Error in sending password reset link',
                'error' => $e->getMessage()
            ], 500);
        }
    }

    /**
     * Reset password
     *
     * @param \App\Http\Requests\Auth\ResetPasswordRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function resetPassword(ResetPasswordRequest $request)
    {
        try {
            $request->validated();
            $status = Password::reset(
                $request->only('email', 'password', 'password_confirmation', 'token'),
                function ($user, $password) {
                    $user->forceFill([
                        'password' => bcrypt($password)
                    ])->save();
                    activity($user->name)
                    ->causedBy($user)
                    ->performedOn($user)
                    ->event('reset_password')
                    ->log(':causer.name reset the password for :subject.email');
                }
            );
            if ($status === Password::PASSWORD_RESET) {
                return response()->json([
                    'status' => 200,
                    'message' => 'Password reset successfully'
                ], 200);
            } else {
                return response()->json([
                    'status' => 500,
                    'message' => 'Error in resetting password'
                ], 500);
            }
        } catch (\Exception $e) {
            return response()->json([
                'status' => 500,
                'message' => 'Error in resetting password',
                'error' => $e->getMessage()
            ]);
        }
    }
}
