<?php

namespace App\Http\Controllers;

use App\Http\Requests\Auth\UserLoginRequest;
use App\Http\Requests\Auth\UserRegisterRequest;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use DB;

class UserAuthController extends Controller
{
    /**
     * Register a new user
     *
     * @param \App\Http\Requests\Auth\UserRegisterRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(UserRegisterRequest $request)
    {
        $request->validated();
        try {
            DB::beginTransaction();
            $user = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => bcrypt($request->password)
            ]);
            $user->assignRole('user');
            $user->givePermissionTo('create post');
            $user->givePermissionTo('update post');
            $user->givePermissionTo('delete post');
            activity($user->name)
                ->causedBy($user)
                ->performedOn($user)
                ->log(':causer.name registered a user: :subject.name');
            DB::commit();
            return response()->json([
                'status' => 201,
                'message' => 'User registered successfully',
            ], 201);
        } catch (\Exception $e) {
            DB::rollBack();
        }
    }

    /**
     * Login a user
     *
     * @param \App\Http\Requests\Auth\UserLoginRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(UserLoginRequest $request)
    {
        $request->validated();
        if (!Auth::attempt($request->only('email', 'password'))) {
            throw new AuthenticationException('Invalid credentials');
        }
        try{
            DB::beginTransaction();
            $user = User::where('email', $request->email)->first();
            $token = $user->createToken('auth_token')->plainTextToken;
            activity($user->name)
                ->causedBy($user)
                ->performedOn($user)
                ->log(':causer.name logged in');
            DB::commit();
            return response()->json([
                'status' => 200,
                'message' => 'User logged in successfully',
                'data' => [
                    'token' => $token
                ]
            ], 200);
        }catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                'status' => 500,
                'message' => 'Error in logging in user',
                'errors' => $e->getMessage()
            ], 500);
        }
    }

    /**
     * Logout a user
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout(Request $request)
    {
        try {
            DB::beginTransaction();
            $request->user()->currentAccessToken()->delete();
            activity($request->user()->name)
                ->causedBy($request->user())
                ->performedOn($request->user())
                ->log(':causer.name logged out');
            DB::commit();
            return response()->json([
                'status' => 200,
                'message' => 'User logged out successfully'
            ], 200);
        } catch (\Exception $e) {
            DB::rollBack();
        }
    }

    /**
     * Get the authenticated user
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function user(Request $request)
    {
        return response()->json([
            'status' => 200,
            'message' => 'User details',
            'data' => [
                'user' => $request->user()
            ]
        ], 200);
    }
}
