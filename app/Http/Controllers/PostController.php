<?php

namespace App\Http\Controllers;

use App\Http\Requests\Post\CreatePostRequest;
use App\Http\Requests\Post\UpdatePostRequest;
use App\Http\Resources\PostSaveResource;
use App\Notifications\PostCreatedNotification;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\Post;
use App\Http\Resources\PostResource;
use Notification;
use Spatie\QueryBuilder\QueryBuilder;
use App\Models\Category;
use App\Models\User;
use Str;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $posts = QueryBuilder::for(Post::class)
            ->allowedFilters(['title', 'category.name', 'user.name'])
            ->allowedSorts(['title', 'created_at', 'updated_at'])
            ->with(['category', 'user', 'media', 'postTranslations'])
            ->jsonPaginate(10);
        return response()->json([
            'status' => 200,
            'message' => 'Posts retrieved successfully',
            'data' => PostResource::collection($posts)
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(CreatePostRequest $request)
    {
        try {
            $request->validated();
            $image = $request->image;
            DB::beginTransaction();
            $post = Post::create([
                'title' => $request->title,
                'category_id' => $request->category_id,
                'user_id' => auth()->id(),
                'content' => [
                    'en' => $request->content_en,
                    'id' => $request->content_id
                ]
            ]);
            $post->addMedia($image)
                ->toMediaCollection('post-image');
            if (auth()->user()->hasRole('user')) {
                Notification::send(auth()->user(), new PostCreatedNotification($request->title, $request->content_en));
            }
            activity(auth()->user()->name)
                ->causedBy(auth()->user())
                ->performedOn($post)
                ->event('created')
                ->log(':causer.name created a post: :subject.title');
            DB::commit();
            return response()->json([
                'status' => 201,
                'message' => 'Post created successfully',
                'data' => new PostSaveResource($post)
            ], 201);
        } catch (\Exception $e) {
            DB::rollBack();
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(Post $post)
    {
        return response()->json([
            'status' => 200,
            'message' => 'Post retrieved successfully',
            'data' => new PostResource($post)
        ], 200);
    }


    /**
     * Update the specified resource in storage.
     */
    public function update(UpdatePostRequest $request, Post $post)
    {
        try {
            $request->validated();
            $image = $request->image;
            $original = $post->getOriginal();

            if (auth()->user()->id !== $post->user_id && !auth()->user()->hasRole('admin')){
                throw new AuthorizationException('You are not authorized to update this post', 403);
            }
            DB::beginTransaction();
            $post->update([
                'title' => $request->title,
                'category_id' => $request->category_id,
                'content' => [
                    'en' => $request->content_en,
                    'id' => $request->content_id
                ]
            ]);
            if ($image) {
                $post->clearMediaCollection('post-image');
                $post->addMedia($image)
                    ->toMediaCollection('post-image');
            }
            activity(auth()->user()->name)
                ->causedBy(auth()->user())
                ->performedOn($post)
                ->event('updated')
                ->withProperties(['new' => $post->getChanges(), 'old' => $original])
                ->log(':causer.name updated the post: :subject.title');
            DB::commit();
            return response()->json([
                'status' => 200,
                'message' => 'Post updated successfully',
                'data' => new PostSaveResource($post)
            ], 200);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                'status' => $e->getCode(),
                'message' => $e->getMessage()
            ], $e->getCode());
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Post $post)
    {
        try {
            DB::beginTransaction();
            $user = auth()->user();
            if ($user->id !== $post->user_id && !$user->hasRole('admin')) {
                throw new \Exception('You are not authorized to delete this post', 403);
            }
            dd($user, $post->user_id, $user->hasRole('admin'));
            $post->delete();
            activity(auth()->user()->name)
                ->causedBy(auth()->user())
                ->performedOn($post)
                ->event('deleted')
                ->log(':causer.name deleted the post: :subject.title');
            DB::commit();
            return response()->json([
                'status' => 200,
                'message' => 'Post deleted successfully'
            ], 200);
        } catch (\Exception $e) {
            DB::rollBack();
        }
    }
}
