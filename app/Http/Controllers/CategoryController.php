<?php

namespace App\Http\Controllers;

use App\Http\Requests\Category\CreateCategoryRequest;
use App\Http\Requests\Category\UpdateCategoryRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Http\Resources\CategoryResource;
use Illuminate\Support\Facades\DB;


class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $categories = Category::all();
        return response()->json([
            'status' => 200,
            'message' => 'Categories retrieved successfully',
            'data' => CategoryResource::collection($categories)
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(CreateCategoryRequest $request)
    {
        try {
            $request->validated();
            DB::beginTransaction();
            $category = Category::create([
                'name' => $request->name,
                'description' => $request->description
            ]);
            activity(auth()->user()->name)
                ->causedBy(auth()->user())
                ->performedOn($category)
                ->event('created')
                ->log(':causer.name created a category: :subject.name');
            DB::commit();
            return response()->json([
                'status' => 201,
                'message' => 'Category created successfully',
                'data' => new CategoryResource($category)
            ], 201);
        } catch (\Exception $e) {
            DB::rollBack();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Category $category
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Category $category)
    {
        return response()->json([
            'status' => 200,
            'message' => 'Category retrieved successfully',
            'data' => new CategoryResource($category)
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \App\Http\Requests\Category\UpdateCategoryRequest $request
     * @param \App\Models\Category $category
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateCategoryRequest $request, Category $category)
    {
        try {
            $request->validated();
            $original = $category->getOriginal();
            DB::beginTransaction();
            $category->update([
                'name' => $request->name,
                'description' => $request->description
            ]);
            activity(auth()->user()->name)
                ->causedBy(auth()->user())
                ->performedOn($category)
                ->event('updated')
                ->withProperties(['new' => $category->getChanges(), 'old' => $original])
                ->log(':causer.name updated a category: :subject.name');
            DB::commit();
            return response()->json([
                'status' => 200,
                'message' => 'Category updated successfully',
                'data' => new CategoryResource($category)
            ], 200);
        } catch (\Exception $e) {
            DB::rollBack();
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Category $category)
    {
        try {
            DB::beginTransaction();
            $category->delete();
            activity(auth()->user()->name)
                ->causedBy(auth()->user())
                ->performedOn($category)
                ->event('deleted')
                ->log(':subject.name deleted a category: :causer.name');
            DB::commit();
            return response()->json([
                'status' => 200,
                'message' => 'Category deleted successfully',
            ], 200);
        } catch (\Exception $e) {
            DB::rollBack();
        }
    }
}
