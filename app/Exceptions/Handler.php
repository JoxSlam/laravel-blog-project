<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Throwable;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Arr;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;

class Handler extends ExceptionHandler
{
    /**
     * The list of the inputs that are never flashed to the session on validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     */
    public function register(): void
    {
        $this->renderable(function (Throwable $e) {
            return response()->json($this->exceptionToArray($e), $this->getApiErrorCode($e));
        });
    }

    public function exceptionToArray(Throwable $e): array
    {
        if (config('app.debug', false)) {
            return [
                'status' => $this->getApiErrorCode($e),
                'message' => ($e instanceof ModelNotFoundException) ? 'Resource not found' : $e->getMessage(),
                'errors' => $e->getMessage(),
                'exception' => get_class($e),
                'file' => $e->getFile(),
                'line' => $e->getLine(),
                'trace' => collect($e->getTrace())->map(static function ($trace): array {
                    return Arr::except($trace, ['args']);
                })->all(),
            ];
        }
        return [
            'status' => $this->getApiErrorCode($e),
            'message' => ($e instanceof HttpExceptionInterface) ? 'Resource not found' : $e->getMessage(),
            'errors' => ($e instanceof ValidationException) ? $e->errors() : []
        ];
    }

    public function getApiErrorCode(Throwable $e): int
    {
        if ($e instanceof ValidationException) {
            return 422;
        }

        if ($e instanceof ModelNotFoundException) {
            return 404;
        }

        if ($e instanceof HttpExceptionInterface) {
            return $e->getStatusCode();
        }

        if ($e instanceof AuthenticationException) {
            return 401;
        }

        if ($e instanceof AuthorizationException) {
            return 403;
        }

        return 500;
    }
}
