<?php

namespace App\Models;

use App\Traits\ClearsResponseCache;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;
use App\Models\User;
use App\Models\Category;
use App\Models\PostTranslations;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\Image\Enums\Fit;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use RichanFongdasen\I18n\Contracts\TranslatableModel;
use RichanFongdasen\I18n\Eloquent\Concerns\Translatable;

class Post extends Model implements HasMedia, TranslatableModel
{
    use HasFactory, HasSlug, InteractsWithMedia, Translatable, ClearsResponseCache;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = ['title', 'category_id', 'user_id', 'slug'];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    /**
     * It is required to define all
     * the translatable attribute names here.
     *
     * @var string[]
     */
    protected array $translates = [
        'content',
    ];

    /**
     * Hides the translations attribute, so it won't present
     * when the model is serialized into an array or JSON document.
     * @var string[]
     */
    protected $hidden = ['translations'];

    /**
     * Get the translated `content` attribute.
     *
     * @return string
     * @throws \ErrorException
     */
    public function getContentAttribute(): string
    {
        return (string) $this->getAttribute('content');
    }

    /**
     * Get the category that owns the post.
     */
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    /**
     * Get the user that owns the post.
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function PostTranslations()
    {
        return $this->hasMany(PostTranslations::class);
    }

    /**
     * Get the options for generating the slug.
     */
    public function getSlugOptions(): SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('title')
            ->saveSlugsTo('slug');
    }

    /**
     * Get the route key for the model.
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }

    /**
     * Get the post's image path.
     */
    public function getImagePathAttribute()
    {
        return asset('storage/' . $this->image);
    }

    public function registerMediaConversions(Media $media = null): void
    {
        $this
            ->addMediaConversion('preview')
            ->fit(Fit::Contain, 300, 300);
    }

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('post-image')
            ->singleFile();
    }

}
