<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PostTranslations extends Model
{
    use HasFactory;

    protected $table = 'post_translations';
    protected $fillable = ['content'];

    protected $hidden = ['id', 'post_id', 'created_at', 'updated_at'];
    public function post()
    {
        return $this->belongsTo(Post::class);
    }


}
