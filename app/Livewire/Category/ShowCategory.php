<?php

namespace App\Livewire\Category;

use Livewire\Component;
use App\Models\Category;

class ShowCategory extends Component
{
    public function delete(Category $category)
    {
        if ($category->delete()) {
            session()->flash('success', 'Category has been deleted');
        } else {
            session()->flash('error', 'Category cannot be deleted');
        }
    }
    public function render()
    {
        return view('livewire.category.show-category')->with([
            'categories' => Category::all()
        ]);
    }
}
