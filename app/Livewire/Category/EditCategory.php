<?php

namespace App\Livewire\Category;

use Livewire\Component;
use App\Models\Category;
use Livewire\Attributes\Validate;

class EditCategory extends Component
{
    #[Validate('required|string|min:3|max:255')]
    public $name = '';

    #[Validate('required|string|min:3|max:255')]
    public $description = '';

    public Category $category;

    public function mount(Category $slug)
    {
        $this->category = $slug;
        $this->name = $slug->name;
        $this->description = $slug->description;
    }

    public function update()
    {
        $this->validate();

        try {
            $this->category->update([
                'name' => $this->name,
                'description' => $this->description
            ]);
            session()->flash('success', 'Category updated successfully');
            $this->redirect(route('category'), true);
        } catch (\Exception $e) {
            session()->flash('error', $e->getMessage());
        }
    }

    public function render()
    {
        return view('livewire.category.edit-category');
    }
}
