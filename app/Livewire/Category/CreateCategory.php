<?php

namespace App\Livewire\Category;

use Livewire\Attributes\Validate;
use Livewire\Component;
use App\Models\Category;

class CreateCategory extends Component
{
    #[Validate('required|string|min:3|max:255')]
    public $name = '';

    #[Validate('required|string|min:3|max:255')]
    public $description = '';

    public function store()
    {
        $this->validate();

        if (
            Category::create([
                'name' => $this->name,
                'description' => $this->description
            ])
        ) {
            session()->flash('success', 'Category has been created');
        } else {
            session()->flash('error', 'Category cannot be created');
        }
        $this->redirect(route('category'), true);
    }
    public function render()
    {
        return view('livewire.category.create-category');
    }
}
