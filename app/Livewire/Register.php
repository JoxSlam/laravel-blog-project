<?php

namespace App\Livewire;

use Livewire\Attributes\Validate;
use Livewire\Component;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class Register extends Component
{
    #[Validate('required')]
    public $name = '';

    #[Validate('required|email|unique:users,email')]
    public $email = '';

    #[Validate('required|min:8')]
    public $password = '';

    #[Validate('required|same:password')]
    public $password_confirmation = '';

    public function register()
    {
        $this->validate();
        try {
            DB::beginTransaction();
            $user = User::create([
                'name' => $this->name,
                'email' => $this->email,
                'password' => bcrypt($this->password)
            ]);
            $user->assignRole('user');
            $user->givePermissionTo('create post');
            $user->givePermissionTo('update post');
            $user->givePermissionTo('delete post');
            activity($user->name)
                ->causedBy($user)
                ->performedOn($user)
                ->log(':causer.name registered a user: :subject.name');
            DB::commit();
            session()->flash('success', 'User registered successfully');
            return $this->redirect(route('login'), true);
        } catch (\Exception $e) {
            DB::rollBack();
            session()->flash('error', 'Error in registering user');
            return $this->redirect(route('register'), true);
        }
    }

    public function render()
    {
        return view('livewire.register');
    }
}
