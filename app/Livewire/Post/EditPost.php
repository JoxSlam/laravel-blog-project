<?php

namespace App\Livewire\Post;

use App\Models\Post;
use Livewire\Component;
use Livewire\Attributes\Validate;
use Livewire\WithFileUploads;
use App\Models\Category;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

class EditPost extends Component
{
    use WithFileUploads;

    #[Validate('required|string|min:3|max:255')]
    public $title = '';

    #[Validate('required|string|min:3')]
    public $content_en = '';

    #[Validate('required|string|min:3')]
    public $content_id = '';

    #[Validate('required|exists:categories,id')]
    public $category_id = '';

    public $image;

    #[Validate('image|max:5000|nullable')]
    public $newImage = null;

    public Post $post;

    public $categories = [];
    public function mount(Post $slug)
    {
        $this->post = $slug;
        $content = $this->post->getAllTranslationValues();
        $this->post->load('category');
        $this->title = $this->post->title;
        $this->content_en = $content['content']['en'];
        $this->content_id = $content['content']['id'];
        $this->category_id = $this->post->category_id;
        $this->categories = Category::all();
        $this->image = $this->post->getFirstMediaUrl('post-images');
    }

    public function update()
    {
        $this->validate();
        try {
            DB::beginTransaction();
            $this->post->update([
                'title' => $this->title,
                'content' => [
                    'en' => $this->content_en,
                    'id' => $this->content_id
                ],
                'slug' => Str::slug($this->title),
                'category_id' => $this->category_id,
            ]);
            if ($this->newImage) {
                $this->post->clearMediaCollection('post-images');
                $this->post->addMedia($this->newImage)
                    ->toMediaCollection('post-images');
            }
            session()->flash('success', 'Post updated successfully');
            DB::commit();
            $this->redirect(route('post.show', $this->post->slug), true);
        } catch (\Exception $e) {
            DB::rollBack();
            session()->flash('error', $e->getMessage());
        }
    }
    public function render()
    {
        return view('livewire.post.edit-post');
    }
}
