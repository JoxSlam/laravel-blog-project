<?php

namespace App\Livewire\Post;

use Livewire\Component;
use App\Models\Post;

class DetailPost extends Component
{
    public Post $post;
    public $image;

    public function mount(Post $slug)
    {
        $this->post = $slug;
        $this->post->load('category', 'user');
        $this->image = $this->post->getFirstMediaUrl('post-image');
    }

    public function render()
    {
        return view('livewire.post.detail-post');
    }
}
