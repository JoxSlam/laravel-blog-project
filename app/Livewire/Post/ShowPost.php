<?php

namespace App\Livewire\Post;

use Livewire\Component;
use App\Models\Post;
use Livewire\WithPagination;

class ShowPost extends Component
{
    use WithPagination;

    public $query;

    public function search()
    {
        $this->resetPage();
    }
    public function delete(Post $post)
    {
        $post->delete();
    }
    public function render()
    {
        return view('livewire.post.show-post', [
            'posts' => Post::where('title', 'like', '%' . $this->query . '%')->paginate(10)
        ]);
    }
}
