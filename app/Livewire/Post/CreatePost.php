<?php

namespace App\Livewire\Post;

use Livewire\Attributes\Validate;
use Livewire\Component;
use App\Models\Post;
use App\Models\Category;
use Illuminate\Support\Facades\DB;
use Livewire\WithFileUploads;


class CreatePost extends Component
{
    use WithFileUploads;

    #[Validate('required|string|min:3|max:255')]
    public $title = '';

    #[Validate('required|string|min:3')]
    public $content_en = '';

    #[Validate('required|string|min:3')]
    public $content_id = '';

    #[Validate('required|exists:categories,id')]
    public $category_id = '';

    #[Validate('required|image|max:5000')]
    public $image;

    public $categories = [];

    public function mount()
    {
        $this->categories = Category::all();
    }

    public function store()
    {
        $this->validate();
        try {
            DB::beginTransaction();
            $post = Post::create([
                'title' => $this->title,
                'content_en' => $this->content_en,
                'content' => [
                    'id' => $this->content_id,
                    'en' => $this->content_en
                ],
                'category_id' => $this->category_id,
                'user_id' => auth()->id(),
            ]);

            $post->addMedia($this->image->getRealPath())
                ->toMediaCollection('post-images');

            session()->flash('success', 'Post created successfully');
            $this->reset();
            DB::commit();

            $this->redirect(route('post'), true);
        } catch (\Exception $e) {
            session()->flash('error', $e->getMessage());
            DB::rollBack();
            $this->redirect(route('post.create'), true);
        }
    }
    public function render()
    {
        return view('livewire.post.create-post')->with('categories', $this->categories);
    }
}
