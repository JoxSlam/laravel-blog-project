<?php

namespace App\Livewire;

use Livewire\Component;
use Illuminate\Support\Facades\Auth;

class Logout extends Component
{
    public function logout()
    {
        auth()->user()->tokens()->delete();
        auth()->guard('web')->logout();
        session()->invalidate();
        session()->regenerateToken();
        session()->flash('success', 'Logged out successfully');
        return $this->redirect(route('login'), true);
    }
    public function render()
    {
        return view('livewire.logout');
    }
}
