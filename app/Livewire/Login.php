<?php

namespace App\Livewire;

use Livewire\Component;
use Livewire\Attributes\Validate;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class Login extends Component
{
    #[Validate('required|email')]
    public $email = '';

    #[Validate('required|min:8')]
    public $password = '';

    public function login()
    {
        $this->validate();
        if (Auth::attempt(['email' => $this->email, 'password' => $this->password])) {
            $user = auth()->user();
            if ($user->tokens()->count() > 0) {
                $user->tokens()->delete();
            }
            if ($user->isAdmin()) {
                $token = $user->createToken('AUTH_TOKEN', ['admin'])->plainTextToken;
            } else {
                $token = $user->createToken('AUTH_TOKEN')->plainTextToken;
            }
            session(['token' => $token]);
            session()->flash('success', 'Logged in successfully');
            return $this->redirect(route('post'), true);
        } else {
            session()->flash('error', 'Invalid email or password');
            return $this->redirect(route('login'), true);
        }
    }

    public function render()
    {
        return view('livewire.login');
    }
}
