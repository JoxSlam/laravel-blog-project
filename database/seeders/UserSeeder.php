<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Eloquent\Factories\Sequence;
use Illuminate\Database\Seeder;
use App\Models\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Role::create(['name' => 'admin']);
        Role::create(['name' => 'user']);
        Permission::create(['name' => 'create post']);
        Permission::create(['name' => 'update post']);
        Permission::create(['name' => 'delete post']);
        Permission::create(['name' => 'create category']);
        Permission::create(['name' => 'update category']);
        Permission::create(['name' => 'delete category']);
        Permission::create(['name' => 'create user']);
        Permission::create(['name' => 'update user']);
        Permission::create(['name' => 'delete user']);

        $admin = User::create([
            'name' => 'Admin',
            'email' => 'admin@admin.com',
            'password' => bcrypt('password'),
        ]);
        $admin->assignRole('admin');
        $admin->givePermissionTo('create post');
        $admin->givePermissionTo('update post');
        $admin->givePermissionTo('delete post');
        $admin->givePermissionTo('create category');
        $admin->givePermissionTo('update category');
        $admin->givePermissionTo('delete category');
        $admin->givePermissionTo('create user');
        $admin->givePermissionTo('update user');
        $admin->givePermissionTo('delete user');

        User::factory()
            ->count(10)
            ->create()
            ->each(function ($user) {
                $user->assignRole('user');
                $user->givePermissionTo('create post');
                $user->givePermissionTo('update post');
                $user->givePermissionTo('delete post');
            });
    }
}
