<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\Post;
use Illuminate\Database\Seeder;
use App\Models\Category;
use Illuminate\Support\Facades\DB;
use Database\Seeders\UserSeeder;



class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $this->call([
            UserSeeder::class,
        ]);
        Category::create(['name' => 'Category 1', 'description' => 'Category 1 description']);
        Category::create(['name' => 'Category 2', 'description' => 'Category 2 description']);

        DB::table('languages')->insert([
            [
                'id' => 1,
                'name' => 'English',
                'language' => 'en',
                'country' => 'US',
                'order' => 1,
            ],
            [
                'id' => 2,
                'name' => 'Indonesian',
                'language' => 'id',
                'country' => 'ID',
                'order' => 2,
            ]
        ]);

        foreach (range(1, 20) as $index) {
            Post::create([
                'title' => "Post $index",
                'category_id' => rand(1, 2),
                'user_id' => rand(1, 2),
                'content' => [
                    'en' => "Content EN $index",
                    'id' => "Content ID $index"
                ]
            ]);
        }
    }
}
