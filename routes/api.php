<?php

use App\Http\Controllers\NewPasswordController;
use App\Http\Controllers\UserAuthController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\ActivityController;
use App\Http\Controllers\ExportController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::post('/register', [UserAuthController::class, 'register'])->name('api.register');
Route::post('/login', [UserAuthController::class, 'login'])->name('api.login');
Route::post('/forgot-password', [NewPasswordController::class, 'forgotPassword'])->name('api.forgot-password');
Route::post('/reset-password', [NewPasswordController::class, 'resetPassword'])->name('api.reset-password');

Route::resource('category', CategoryController::class)->only(['index', 'show'])->names([
    'index' => 'api.category.index',
    'show' => 'api.category.show'
]);
Route::resource('post', PostController::class)->only(['index', 'show'])->names([
    'index' => 'api.post.index',
    'show' => 'api.post.show'
]);

Route::middleware('auth:sanctum')->group(function () {
    Route::delete('/logout', [UserAuthController::class, 'logout'])->name('api.logout');
    Route::get('/user', [UserAuthController::class, 'user'])->name('api.user');
});

Route::middleware(['auth:sanctum', 'role:admin'])->group(function () {
    Route::resource('category', CategoryController::class)->except(['index', 'show'])
        ->names([
            'create' => 'api.category.create',
            'store' => 'api.category.store',
            'update' => 'api.category.update',
            'destroy' => 'api.category.destroy'
        ]);

    Route::get('/activity-log', [ActivityController::class, 'search'])->middleware('doNotCacheResponse')->name('api.activity-log.index');
});
Route::get('/export/users', [ExportController::class, 'exportUsers'])->middleware('doNotCacheResponse')->name('api.export.users');
Route::get('/export/posts', [ExportController::class, 'exportPosts'])->middleware('doNotCacheResponse')->name('api.export.posts');
Route::get('/export/categories', [ExportController::class, 'exportCategories'])->middleware('doNotCacheResponse')->name('api.export.categories');

Route::middleware(['auth:sanctum', 'role:admin|user'])->group(function () {
    Route::resource('post', PostController::class)->except(['index', 'show'])
        ->names([
            'index' => 'api.post.index',
            'show' => 'api.post.show',
            'create' => 'api.post.create',
            'update' => 'api.post.update',
            'destroy' => 'api.post.destroy'
        ]);
});
