<?php

use App\Livewire\Category\ShowCategory;
use App\Livewire\Category\CreateCategory;
use App\Livewire\Category\EditCategory;
use App\Livewire\Login;
use App\Livewire\Register;
use App\Livewire\Post\ShowPost;
use App\Livewire\Post\CreatePost;
use App\Livewire\Post\DetailPost;
use App\Livewire\Post\EditPost;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::middleware('doNotCacheResponse')->group(function () {
    Route::get('/login', Login::class)->name('login');
    Route::get('/register', Register::class)->name('register');

    Route::middleware(['auth:sanctum'])->group(function () {
        Route::get('/post', ShowPost::class)->name('post');
        Route::get('/post/create', CreatePost::class)->name('post.create');
        Route::get('/post/{slug}', DetailPost::class)->name('post.show');
        Route::get('/post/{slug}/edit', EditPost::class)->name('post.edit');

        Route::get('/category', ShowCategory::class)->name('category');
        Route::get('/category/create', CreateCategory::class)->name('category.create');
        Route::get('/category/{slug}/edit', EditCategory::class)->name('category.edit');
    });
});

