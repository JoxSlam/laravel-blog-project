<div class="d-flex justify-content-center" style="">

    <form class="bg-primary-subtle p-4 mt-4 rounded-1 bg-dark-subtle" wire:submit='login'>
        @if (session()->has('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif
        @if (session()->has('error'))
            <div class="alert alert-danger">
                {{ session('error') }}
            </div>
        @endif
        <div class="mb-3">
            <label for="email" class="form-label">Email address</label>
            <input type="email" class="form-control" id="email" wire:model="email">
            @error('email')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="mb-3">
            <label for="password" class="form-label">Password</label>
            <input type="password" class="form-control" id="password" wire:model="password">
            @error('password')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <p>Don't have an account? <a href="{{ route('register') }}" wire:navigate>Register</a></p>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>
