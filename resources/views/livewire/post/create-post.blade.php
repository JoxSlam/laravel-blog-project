<div class="container">
    <div class="py-2">
        <h2 class="text-3xl font-bold text-white mb-0">Create Post</h2>
        @if (session('success'))
            <div class="alert alert-success alert-dismissible fade show m-0 w-full" role="alert">
                {{ session('success') }}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        @endif
        @if (session('error'))
            <div class="alert alert-danger alert-dismissible fade show m-0 w-full" role="alert">
                {{ session('error') }}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        @endif
    </div>
    <form wire:submit.prevent="store" class="w-full p-4 bg-secondary-subtle rounded">
        <div class="form-group">
            <label for="title">Title</label>
            <input type="text" wire:model="title" class="form-control" id="title" placeholder="Enter title">
            @error('title')
                <span class="text-danger">{{ $message }}</span>
            @enderror
        </div>
        <div class="form-group">
            <label for="content_en">English Content</label>
            <textarea wire:model="content_en" class="form-control" id="content_en" placeholder="Enter content english"></textarea>
            @error('content_en')
                <span class="text-danger">{{ $message }}</span>
            @enderror
        </div>
        <div class="form-group">
            <label for="content_id">Indonesia Content</label>
            <textarea wire:model="content_id" class="form-control" id="content_id" placeholder="Enter content Indonesia"></textarea>
            @error('content_id')
                <span class="text-danger">{{ $message }}</span>
            @enderror
        </div>
        <div class="form-group">
            <label for="category_id">Category</label>
            <select wire:model="category_id" class="form-control" id="category_id">
                <option value="">Select Category</option>
                @foreach ($categories as $category)
                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                @endforeach
            </select>
            @error('category_id')
                <span class="text-danger">{{ $message }}</span>
            @enderror
        </div>
        <div class="form-group row">
            <label for="image">Image</label>
            <input type="file" wire:model="image" class="form-control-file" id="image">
            @error('image')
                <span class="text-danger">{{ $message }}</span>
            @enderror

            @if ($image)
                <div class="mt-2" style="width: 200px;">
                    <label>Image Preview</label>
                    <img src="{{ $image->temporaryUrl() }}" class="img-fluid img-thumbnail" alt="Image"
                        loading="lazy">
                </div>
            @endif
        </div>
        <button type="submit" class="btn btn-primary my-2">Submit</button>
    </form>
</div>
