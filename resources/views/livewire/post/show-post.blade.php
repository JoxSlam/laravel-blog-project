<div class="w-full py-4">
    <div class="py-1">
        <h1 class="text-3xl font-bold text-white">Posts</h1>
        @if (session('success'))
            <div class="alert alert-success alert-dismissible fade show m-0 w-full" role="alert">
                {{ session('success') }}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        @endif
        @if (session('error'))
            <div class="alert alert-danger alert-dismissible fade show m-0 w-full" role="alert">
                {{ session('error') }}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        @endif
        <a href="{{ route('post.create') }}" wire:navigate class="btn btn-primary">Create Post</a>
    </div>
    <div class="py-1">
        <input type="text" class="form-control" placeholder="Search" wire:model="query" wire:keydown="search">
    </div>
    @if ($posts->isEmpty())
        <div class="alert alert-warning alert-dismissible fade show m-0 w-full" role="alert">
            No posts found.
        </div>
    @endif
    @foreach ($posts as $post)
        <div class="container">
            <div class="row align-items-center bg-white text-dark py-1 mb-2 rounded" key="{{ $post->id }}">
                <div class="col">
                    <h5 class="text-dark">{{ $post->title }}</h5>
                    <p class="text-dark">{{ $post->content }}</p>
                    <p class="text-muted">Posted by {{ $post->user->name }} on
                        {{ $post->updated_at->diffForHumans() }}
                    </p>
                </div>
                <div class="col d-flex justify-content-end">
                    <a href="{{ route('post.show', $post->slug) }}" wire:navigate class="btn btn-primary mx-1">View</a>
                    <a href="{{ route('post.edit', $post->slug) }}" wire:navigate class="btn btn-warning mx-1">Edit</a>
                    <button type="button" class="btn btn-danger mx-1" wire:click="delete('{{ $post->slug }}')"
                        wire:confirm="Are you sure you want to delete this post?">
                        Delete
                    </button>
                </div>
            </div>
        </div>
    @endforeach
    {{ $posts->links() }}
</div>
