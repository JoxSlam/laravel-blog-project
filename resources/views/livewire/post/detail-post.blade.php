<div class="container">
    <div class="py-2">
        <a href="{{ route('post') }}" class="text-white bg-primary p-2 rounded hover:bg-primary-dark"
            wire:navigate>Back</a>
    </div>

    <div class="card">
        <div class="card-header">
            <img src="{{ $image }}" alt="Image" class="img-fluid"
                style="width: 100%; height: 300px; object-fit: cover;">
        </div>
        <div class="card-body">
            <h5>{{ $post->title }}</h5>
            <p>{{ $post->content }}</p>
            <p>Category: {{ $post->category->name }}</p>
            <p>Posted by {{ $post->user->name }} on {{ $post->created_at->diffForHumans() }}</p>
        </div>
    </div>
</div>
