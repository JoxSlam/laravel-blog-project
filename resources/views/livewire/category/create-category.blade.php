<div class="container">
    <div class="py-2">
        <h2 class="text-3xl font-bold text-white mb-0">Create Category</h2>
        @if (session('success'))
            <div class="alert alert-success alert-dismissible fade show m-0 w-full" role="alert">
                {{ session('success') }}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        @endif
        @if (session('error'))
            <div class="alert alert-danger alert-dismissible fade show m-0 w-full" role="alert">
                {{ session('error') }}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        @endif
    </div>
    <form wire:submit.prevent="store" class="w-full p-4 bg-secondary-subtle rounded">
        <div class="form-group mb-2">
            <label for="name">Name</label>
            <input type="text" wire:model="name" class="form-control" id="name" placeholder="Enter name">
            @error('name')
                <span class="text-danger">{{ $message }}</span>
            @enderror
        </div>
        <div class="form-group mb-2">
            <label for="description">Description</label>
            <input type="text" wire:model="description" class="form-control" id="description"
                placeholder="Enter description">
            @error('description')
                <span class="text-danger">{{ $message }}</span>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Create</button>
    </form>
</div>
