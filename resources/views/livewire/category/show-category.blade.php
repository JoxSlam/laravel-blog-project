<div class="container my-2 p-4 bg-white rounded">
    <div class="my-2">
        @if (session('success'))
            <div class="alert alert-success alert-dismissible fade show m-0 w-full" role="alert">
                {{ session('success') }}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        @endif
        @if (session('error'))
            <div class="alert alert-danger alert-dismissible fade show m-0 w-full" role="alert">
                {{ session('error') }}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        @endif
    </div>
    <a href="{{ route('category.create') }}" class="btn btn-primary my-2">Create Category</a>
    <table class="table table-striped table-dark">
        <thead>
            <tr>
                <th>No</th>
                <th>Name</th>
                <th>Description</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($categories as $category)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $category->name }}</td>
                    <td>{{ $category->description }}</td>
                    <td>
                        <a href="{{ route('category.edit', $category->slug) }}"
                            class="btn btn-sm btn-primary">Edit</a>
                        <button wire:click="delete('{{ $category->slug }}')"
                            class="btn btn-sm btn-danger">Delete</button>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
