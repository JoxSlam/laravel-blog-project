<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bootstrap demo</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.3/font/bootstrap-icons.min.css">
</head>

<body class="bg-dark">
    @if (auth()->user())
        <div class="container-fluid">
            <div class="row flex-nowrap">
                <div class="col-auto col-md-3 col-xl-2 px-sm-2 px-0 bg-dark-subtle position-sticky"
                    style="height: 100vh; overflow-y: auto; top: 0; z-index: 1000;" id="sidebar">
                    <div
                        class="d-flex flex-column align-items-center align-items-sm-start px-3 pt-2 text-white min-vh-100">
                        <a href="/"
                            class="d-flex align-items-center pb-3 mb-md-0 me-md-auto text-primary text-decoration-none">
                            <span class="fs-5 d-none d-sm-inline">Laravel Blog App</span>
                        </a>
                        <ul class="nav nav-pills flex-column mb-sm-auto mb-0 align-items-center align-items-sm-start"
                            id="menu">
                            <li>
                                <a href="/post" class="nav-link align-middle px-0" wire:navigate>
                                    <i class="fs-4 bi bi-file-post-fill"></i> <span
                                        class="ms-1 d-none d-sm-inline">Posts</span>
                                </a>
                            </li>
                            <li>
                                <a href="/category" class="nav-link align-middle px-0" wire:navigate>
                                    <i class="fs-4 bi bi-bookmarks"></i> <span
                                        class="ms-1 d-none d-sm-inline">Categories</span>
                                </a>
                            </li>
                            <li>
                                <a href="/tags" class="nav-link align-middle px-0" wire:navigate>
                                    <i class="fs-4 bi bi-people"></i> <span class="ms-1 d-none d-sm-inline">Users</span>
                                </a>
                            </li>
                        </ul>
                        <hr>
                        <livewire:logout>
                    </div>
                </div>
                <div class="col-9">
                    {{ $slot }}
                </div>
            </div>
        </div>
    @else
        <div class="container">
            {{ $slot }}
        </div>
    @endif
</body>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.8/dist/umd/popper.min.js"
    integrity="sha384-I7E8VVD/ismYTF4hNIPjVp/Zjvgyol6VFvRkX/vR+Vc4jQkC+hVqc2pM8ODewa9r" crossorigin="anonymous">
</script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.min.js"
    integrity="sha384-0pUGZvbkm6XF6gxjEnlmuGrJXVbNuzT9qBBavbLwCsOGabYfZo0T0to5eqruptLy" crossorigin="anonymous">
</script>

</html>
