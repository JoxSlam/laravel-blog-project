<?php

namespace Tests\Feature;

use App\Models\User;
use Database\Seeders\UserSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Illuminate\Support\Facades\DB;

class AuthTest extends TestCase
{
    public function testAdminLoginFailedWrongPassword()
    {
        $this->post('/api/login', [
            'email' => 'admin@admin.com',
            'password' => 'password123'
        ])
            ->assertStatus(401)
            ->assertJson([
                'status' => 401,
                'message' => 'Invalid credentials',
            ]);
    }
    public function testAdminLoginSuccess()
    {
        $this->seed([UserSeeder::class]);
        $this->post('/api/login', [
            'email' => 'admin@admin.com',
            'password' => 'password'
        ])
            ->assertStatus(200)
            ->assertJson([
                'status' => 200,
                'message' => 'User logged in successfully',
                // 'token' => {{secret-token}}
            ]);
    }
    public function testUserLoginSuccess()
    {
        $this->seed([UserSeeder::class]);
        $this->post('/api/login', [
            'email' => 'user@user.com',
            'password' => 'password'
        ])
            ->assertStatus(200)
            ->assertJson([
                'status' => 200,
                'message' => 'User logged in successfully',
                // 'token' => {{secret-token}}
            ]);
    }
    public function testUserRegisterSuccess()
    {
        $this->seed([UserSeeder::class]);
        $this->post('/api/register', [
            'name' => 'Varian Avila',
            'email' => 'varian236@gmail.com',
            'password' => 'password',
            'password_confirmation' => 'password'
        ])
            ->assertStatus(201)
            ->assertJson([
                'status' => 201,
                'message' => 'User registered successfully'
            ]);
    }
    public function testUserRegisterFailedEmailExist()
    {
        $this->seed([UserSeeder::class]);
        $this->post('/api/register', [
            'name' => 'Varian Avila',
            'email' => 'user@user.com',
            'password' => 'password123',
            'password_confirmation' => 'password123'
        ])->assertStatus(422)
            ->assertJson([
                'status' => 422,
                'message' => 'Validation error',
                'errors' => [
                    'email' => ['The email has already been taken.']
                ]
            ]);
    }
    public function testUserRegisterFailedPasswordMismatch()
    {
        $this->seed([UserSeeder::class]);
        $this->post('/api/register', [
            'name' => 'Varian Avila',
            'email' => 'varian236@gmail.com',
            'password' => 'password',
            'password_confirmation' => 'password123'
        ])->assertStatus(422)
            ->assertJson([
                'status' => 422,
                'message' => 'Validation error',
                'errors' => [
                    'password' => ['The password field confirmation does not match.']
                ]
            ]);
    }
}
